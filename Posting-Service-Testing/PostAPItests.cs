using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Posting_Service;
using Posting_Service.Models;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Posting_Service_Testing
{
	[TestClass]
	public class PostAPItests
	{
		private static HttpClient client;
		static Post post = new Post();

		[ClassInitialize]
		public static void prep(TestContext context)
		{
			var appFactory = new WebApplicationFactory<Startup>();
			client = appFactory.CreateClient();
			client.BaseAddress = new System.Uri("http://127.0.0.1:6000/api/post");

			post.id = "testID";
			post.userId = "testID";
			post.username = "testusername1234";
			post.postContent = "TestMessage from backend testing";
		}

		[TestMethod]
		public async Task GetAllTestOk()
		{
			var response = await client.GetAsync(client.BaseAddress);

			Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
		}

		[TestMethod]
		public async Task PutNoContent()
		{

			HttpRequestMessage request = new HttpRequestMessage
			{
				Content = new StringContent(JsonConvert.SerializeObject(post), Encoding.UTF8, "application/json"),
				Method = HttpMethod.Put,
				RequestUri = client.BaseAddress
			};
			var response = await client.SendAsync(request);

			Assert.AreEqual(System.Net.HttpStatusCode.NoContent, response.StatusCode);
		}

		[TestMethod]
		public async Task GetByIdBadRequest()
		{
			var request = "http://127.0.0.1:6000/api/post/GetUserPosts?uid=integrationtest123";

			var response = await client.GetAsync(request);

			Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
		}

		[TestMethod]
		public async Task GetByIdOk()
		{
			var request = "http://127.0.0.1:6000/api/post/GetUserPosts?uid=18aa2818-4fe5-4fb5-9a5e-2294bb6ca3fa";

			var response = await client.GetAsync(request);

			Assert.AreEqual(System.Net.HttpStatusCode.OK, response.StatusCode);
		}

		[TestMethod]
		public async Task DeleteOk()
		{
			HttpRequestMessage request = new HttpRequestMessage
			{
				Content = new StringContent(JsonConvert.SerializeObject(post), Encoding.UTF8, "application/json"),
				Method = HttpMethod.Delete,
				RequestUri = client.BaseAddress
			};

			var response = await client.SendAsync(request);

			Assert.AreEqual(System.Net.HttpStatusCode.NoContent, response.StatusCode);
		}
	}
}
